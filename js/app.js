let cbleep=new Audio()
cbleep.src="music/C.wav";  

let dbleep=new Audio()
dbleep.src="music/D.wav";

let ebleep=new Audio()
ebleep.src="music/E.wav";

let fbleep=new Audio()
fbleep.src="music/F.wav"; 

let gbleep=new Audio()
gbleep.src="music/G.wav";

let ableep=new Audio()
ableep.src="music/A.wav";


let bbleep=new Audio()
bbleep.src="music/B.wav";

let c1bleep=new Audio()
c1bleep.src="music/C1.wav"; 

let d1bleep=new Audio()
d1bleep.src="music/D1.wav";

let e1bleep=new Audio()
e1bleep.src="music/E1.wav";


let f1bleep=new Audio()
f1bleep.src="music/F1.wav";


let csbleep=new Audio()
csbleep.src="music/C_s.wav";

let dsbleep=new Audio()
dsbleep.src="music/D_s.wav";


let fsbleep=new Audio()
fsbleep.src="music/F_s.wav";

let gsbleep=new Audio()
gsbleep.src="music/G_s.wav";

let bbbleep=new Audio()
bbbleep.src="music/Bb.wav";

let cs1bleep=new Audio()
cs1bleep.src="music/C_s1.wav";

let ds1bleep=new Audio()
ds1bleep.src="music/D_s1.wav"; 



let mod = document.getElementById('color-mode')
mod.addEventListener('click',(e)=>{
    
    if(mod.innerHTML==='Dark Mode'){
       document.body.style.backgroundColor='#404040';
       let whiteBtn =document.getElementsByClassName('white-btn')
       let blackBtn =document.getElementsByClassName('black-btn')
       for(let i=0;i<whiteBtn.length;i++){
        whiteBtn[i].style.backgroundColor='#404040';
        whiteBtn[i].style.boxShadow="3px 3px 3px #282828, -3px -3px 3px #282828"
       }
       for(let i=0;i<blackBtn.length;i++){
        blackBtn[i].style.boxShadow="3px 3px 3px #282828, -3px -3px 3px #282828"
       }
        mod.innerHTML="Light Mode"
        mod.style.backgroundColor='#404040';
        mod.style.color='#fff';
    }else{
        document.body.style.backgroundColor='#eff0f4'
        mod.style.backgroundColor='#fff';
        mod.style.color='#404040';
        mod.innerHTML="Dark Mode"
       
        let whiteBtn =document.getElementsByClassName('white-btn')
        let blackBtn =document.getElementsByClassName('black-btn')
        for(let i=0;i<whiteBtn.length;i++){
         whiteBtn[i].style.backgroundColor='#eff0f4';
         whiteBtn[i].style.boxShadow="3px 3px 3px #d0d0d0, -3px -3px 3px #f8f8f8"
        }
        for(let i=0;i<blackBtn.length;i++){
         blackBtn[i].style.boxShadow="3px 3px 3px #eff0f4, -3px -3px 3px #eff0f4"
        }
    }
})
